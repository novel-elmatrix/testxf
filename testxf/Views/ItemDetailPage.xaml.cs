﻿using System.ComponentModel;
using testxf.ViewModels;
using Xamarin.Forms;

namespace testxf.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}