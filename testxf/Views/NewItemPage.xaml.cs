﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using testxf.Models;
using testxf.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace testxf.Views
{
    public partial class NewItemPage : ContentPage
    {
        public Item Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();
            BindingContext = new NewItemViewModel();
        }
    }
}